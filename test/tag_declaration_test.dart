import "package:app/entities/tag_declaration.dart";
import "package:flutter_test/flutter_test.dart";

void main() {
  test("Omitted relevance", () {
    expect(
      tagDeclarationsIn("#tag"),
      [
        TagDeclaration(name: "tag", relevanceFormula: ""),
      ],
    );
  });

  test("Number as relevance", () {
    expect(
      tagDeclarationsIn("#tag(42)"),
      [
        TagDeclaration(name: "tag", relevanceFormula: "42"),
      ],
    );
  });

  test("Arithmetic expression as relevance", () {
    expect(
      tagDeclarationsIn("#tag(42 + 10)"),
      [
        TagDeclaration(name: "tag", relevanceFormula: "42 + 10"),
      ],
    );
  });

  test("Text without tags", () {
    expect(
      tagDeclarationsIn("I am not a tag!"),
      isEmpty,
    );
  });

  test("Tags mixed with text", () {
    expect(
      tagDeclarationsIn(
        "Just some random text with"
        " #tags here and #there(10)."
        "\nHow about a word that ends"
        " with a#tag(3)?",
      ),
      [
        TagDeclaration(name: "tags", relevanceFormula: ""),
        TagDeclaration(name: "there", relevanceFormula: "10"),
        TagDeclaration(name: "tag", relevanceFormula: "3"),
      ],
    );
  });

  test("Using other languages", () {
    expect(
      tagDeclarationsIn("#тэг(1) #Γεια #ハロー(5)"),
      [
        TagDeclaration(name: "тэг", relevanceFormula: "1"),
        TagDeclaration(name: "Γεια", relevanceFormula: ""),
        TagDeclaration(name: "ハロー", relevanceFormula: "5"),
      ],
    );
  });

  test("Variables", () {
    expect(
      tagDeclarationsIn("#c(a * b)").getByName("c").variables,
      ["a", "b"],
    );
  });

  test("Implicit tags from variables", () {
    expect(
      tagDeclarationsIn("#c(a * b)"),
      [
        TagDeclaration(name: "a", relevanceFormula: "0"),
        TagDeclaration(name: "b", relevanceFormula: "0"),
        TagDeclaration(name: "c", relevanceFormula: "a * b"),
      ],
    );
  });

  test("Implicit tag and explicit tag", () {
    expect(
      tagDeclarationsIn("#c(3) #d(c + e) #e"),
      [
        TagDeclaration(name: "c", relevanceFormula: "3"),
        TagDeclaration(name: "e", relevanceFormula: "0"),
        TagDeclaration(name: "d", relevanceFormula: "c + e"),
        TagDeclaration(name: "e", relevanceFormula: ""),
      ],
    );
  });
}

TagDeclaration tagDeclarationIn(String s) {
  return tagDeclarationsIn(s).first;
}

extension on List<TagDeclaration> {
  TagDeclaration getByName(String name) {
    return firstWhere((d) => d.name == name);
  }
}
