import "package:app/entities/tag.dart";
import "package:flutter_test/flutter_test.dart";

void main() {
  test("Omitted relevance", () {
    expect(
      tagsIn("#a #b()"),
      {
        Tag(name: "a", relevance: 1),
        Tag(name: "b", relevance: 1),
      },
    );
  });

  test("Number as relevance", () {
    expect(
      tagsIn("#tag(1)"),
      {
        Tag(name: "tag", relevance: 1),
      },
    );
  });

  test("Arithmetic expression as relevance", () {
    expect(
      tagsIn("#tag(2 + 2)"),
      {
        Tag(name: "tag", relevance: 4),
      },
    );
  });

  test("Using variables", () {
    expect(
      tagsIn("#a(3) #b(4) #c(a * b) #d(c + e) #e"),
      {
        Tag(name: "a", relevance: 3),
        Tag(name: "b", relevance: 4),
        Tag(name: "c", relevance: 12),
        Tag(name: "d", relevance: 12),
        Tag(name: "e", relevance: 1),
      },
    );
  });

  test("No infinite recursion", () {
    expect(
      tagsIn("#x(x) #a(5) #a(-3) #d(9999999) #d(-d)"),
      {
        Tag(name: "x", relevance: 0),
        Tag(name: "a", relevance: 2),
        Tag(name: "d", relevance: 0),
      },
    );
  });
}
