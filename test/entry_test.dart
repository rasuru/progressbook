import "package:app/entities/entry.dart";
import "package:flutter_test/flutter_test.dart";

void main() {
  var entry = Entry(
    text: "test entry",
    timestamp: DateTime.now(),
  );

  setUp(() {
    entry = Entry(
      text: """This line is less than 40 characters

Ut et accusantium ipsum ut aut. Tempore perspiciatis placeat quasi qui quo quis qui. Quo dignissimos dolor omnis porro ipsum. Et velit odio voluptates.

Sit placeat quibusdam aut possimus nihil ad. Aut magnam voluptatem quia officiis at quis ducimus. In perspiciatis quia in deleniti. Esse quod sed distinctio nobis iusto. Temporibus molestias sint doloremque sunt iure voluptatum cumque. Ipsum et veniam et aut molestiae facilis.

Voluptatem amet minus consequuntur. Nobis dolorem ea et. Dolorem et assumenda perferendis deserunt reiciendis assumenda necessitatibus labore.

Nihil qui a ut maiores illum et. Sapiente vero nihil vitae. Ipsa dolore blanditiis eos quasi saepe est quisquam expedita. Alias adipisci tempore veniam illum qui consequuntur. Repellendus temporibus voluptas dicta quos.

Illo commodi et atque ut. In officiis sed placeat dolorem rerum maxime. Iste qui accusamus vel. Ut corrupti eos explicabo nemo adipisci nisi. Mollitia omnis sit ad dolores enim facilis.""",
      timestamp: DateTime(2021, 1, 1),
    );
  });

  test("Title is first line", () {
    expect(
      entry.title,
      "This line is less than 40 characters",
    );
  });

  test("Empty title when it's longer than 40 characters", () {
    expect(
      Entry(
        text: "Title Title Title Title Title Title"
            " Title Title Title Title Title Title",
        timestamp: DateTime.now(),
      ).title,
      "",
    );
  });

  test("Long first line is just text body", () {
    final text = "TextBody TextBody TextBody TextBody TextBody"
        " TextBody TextBody TextBody TextBody TextBody";

    expect(
      Entry(
        text: text,
        timestamp: DateTime.now(),
      ).textBody,
      text,
    );
  });
}
