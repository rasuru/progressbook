import "package:app/utils.dart";
import "package:app/use_cases/output/viewed_tag_layers.dart"
    as domain;
import "package:app/ui_models/viewed_tag_layer.dart";

export "package:app/ui_models/viewed_tag_layer.dart";

final viewedTagLayers$ = domain.viewedTagLayers$.newState(
  (layers) => layers.map(ViewedTagLayer.from).toList(),
);
final initialTagLine$ = domain.enabledTagLayers$.newState(
  (layers) => layers
      .map((layer) => layer.text)
      .join(" ")
      .trim()
      .absentIf((s) => s.isEmpty),
);
