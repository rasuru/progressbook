import "package:app/utils/datetime.dart";
import "package:app/utils.dart";
import "package:app/ui_models/viewed_entry.dart";
import "package:app/ui_models/daily_stats_tag.dart";
import "package:app/use_cases/output/viewed_page.dart" as domain;
import "package:intl/intl.dart";

final viewedPage$ = domain.viewedPage$.newState(
  (page) => page.map(ViewedEntry.from).toList(),
);
final dailyStats$ = domain.viewedPageStats$
    .newState((stats) => stats.map(DailyStatsTag.from).toList());

final formatCurrentYearDate =
    DateFormat("E, d MMM", "ru").format;
final formatOtherYearDate = DateFormat("y, d MMM", "ru").format;
final viewedPageDate$ = domain.viewedPageDate$;
final viewedPageDateFormatted$ = viewedPageDate$.newState(
  (date) => isCurrentYear(date.year)
      ? formatCurrentYearDate(date)
      : formatOtherYearDate(date),
);

final priority$ = domain.viewedPage$.map((entries) => entries
    .map((entry) => entry.tags)
    .flattened
    .where((tag) => tag.name == "xp")
    .map((tag) => tag.relevance)
    .sum) as BehaviorSubject<double>;

final priorityFormatted$ = priority$.map(
  (relevance) => "${relevance.withPrecisionOf(2).format()}xp",
);

final selectedEntries$ =
    viewedPage$.newState((_) => <ViewedEntry>{});
final selectModeEnabled$ = selectedEntries$
    .newState((entries) => entries.isNotEmpty)
    .distinctState();

void toggleSelectEntry(ViewedEntry entry) {
  if (current(selectedEntries$).contains(entry)) {
    deselectEntry(entry);
  } else {
    selectEntry(entry);
  }
}

void selectEntry(ViewedEntry entry) {
  selectedEntries$.add(
    current(selectedEntries$).union({entry}),
  );
}

void deselectEntry(ViewedEntry entry) {
  selectedEntries$.add(
    current(selectedEntries$).difference({entry}),
  );
}

bool isEntrySelected(ViewedEntry entry) {
  return current(selectedEntries$).contains(entry);
}

void disableSelectMode() {
  selectedEntries$.add({});
}
