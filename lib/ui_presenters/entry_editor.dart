import "package:app/ui_models/editor_entry.dart";
import "package:app/ui_models/viewed_entry.dart";
import "package:app/utils.dart";

final entry$ = stateOf<EditorEntry>(NewEntry());

void reset() => entry$.add(NewEntry());
void edit(ViewedEntry viewedEntry) =>
    entry$.add(EditedEntry.from(viewedEntry));

final lockedTime$ = entry$.newState((e) => e.getLockedTime());

void resetTime() => lockedTime$.update(None());
void lockTime(DateTime time) => lockedTime$.update(Some(time));

DateTime get time {
  return current(lockedTime$).getOrElse(() => DateTime.now());
}

bool get isEditingExistingEntry =>
    current(entry$) is EditedEntry;

bool isEditing(ViewedEntry entry) {
  final e = current(entry$);

  return e is EditedEntry && e.timestamp == entry.timestamp;
}
