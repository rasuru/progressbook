import "package:app/utils/uuid.dart";

class TagLayer {
  TagLayer({
    required this.id,
    required this.text,
    required this.enabled,
    required this.order,
  });

  static TagLayer generate({
    required String text,
    required int order,
  }) {
    return TagLayer(
      id: generateId(),
      text: text,
      order: order,
      enabled: true,
    );
  }

  final String id;
  final String text;
  final bool enabled;
  final int order;

  TagLayer toggle() => copyWith(enabled: !enabled);

  TagLayer copyWith({
    String? id,
    String? text,
    bool? enabled,
    int? order,
  }) {
    return TagLayer(
      id: id ?? this.id,
      text: text ?? this.text,
      enabled: enabled ?? this.enabled,
      order: order ?? this.order,
    );
  }
}
