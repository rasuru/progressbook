import "package:app/utils.dart";

class TagDeclaration with EquatableMixin {
  TagDeclaration({
    required this.name,
    required this.relevanceFormula,
  });

  static TagDeclaration implicit(String name) {
    return TagDeclaration(name: name, relevanceFormula: "0");
  }

  final String name;
  final String relevanceFormula;

  List<String> get variables {
    return tagNameRegex
        .allMatches(relevanceFormula)
        .map((match) => match.namedGroup("tag")!)
        .toList();
  }

  @override
  String toString() {
    return "#$name($relevanceFormula)";
  }

  @override
  List<Object?> get props => [name, relevanceFormula];
}

final tagNameRegex = RegExp(
  r"(?<tag>(\p{L}|_)+)",
  unicode: true,
);
final tagRelevanceRegex = RegExp(
  r"\((?<relevance>.*?)\)",
  unicode: true,
);
final tagRegex = RegExp(
  "#${tagNameRegex.pattern}"
  "(${tagRelevanceRegex.pattern})?",
  unicode: true,
);

List<TagDeclaration> tagDeclarationsIn(String text) {
  TagDeclaration declarationFromMatch(match) {
    return TagDeclaration(
      name: match.namedGroup("tag")!,
      relevanceFormula: match.namedGroup("relevance") ?? "",
    );
  }

  return tagRegex
      .allMatches(text)
      .map(declarationFromMatch)
      .toList()
      .withImplicitTags();
}

extension on List<TagDeclaration> {
  List<TagDeclaration> withImplicitTags() {
    return fold([], (tagsSoFar, nextDeclaration) {
      return [
        ...tagsSoFar,
        ...tagsSoFar.implicitTagsFrom(nextDeclaration),
        nextDeclaration,
      ];
    });
  }

  List<TagDeclaration> implicitTagsFrom(
    TagDeclaration declaration,
  ) {
    return declaration.variables
        .where(isNewVariable)
        .map(TagDeclaration.implicit)
        .toList();
  }

  bool isNewVariable(String name) => !tagNames.contains(name);

  List<String> get tagNames => map((d) => d.name).toList();
}

bool checkIsValidTagCandidate(String s) {
  s = s.trim();
  final match = tagRegex.firstMatch(s.trim());
  final matchedText = match?.group(0);

  return matchedText == s.trim();
}
