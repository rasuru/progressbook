import "package:app/utils/math_solver.dart";
import "package:app/utils.dart";
import "package:math_expressions/math_expressions.dart";

import "tag_declaration.dart";

class Tag with EquatableMixin {
  Tag({
    required this.name,
    required this.relevance,
  });

  final String name;
  final double relevance;

  Tag addRelevance(double n) => Tag(
        name: name,
        relevance: relevance + n,
      );

  @override
  String toString() {
    return "#$name($relevance)";
  }

  @override
  List<Object?> get props => [name];
}

Set<Tag> tagsIn(String text) {
  return tagDeclarationsIn(text)
      .fold({}, (tagsSoFar, next) => tagsSoFar.addTag(next));
}

extension on Set<Tag> {
  Set<Tag> addTag(TagDeclaration declaration) {
    return {
      ...this,
      declaration.evaluateIn(createMathContext()),
    };
  }

  ContextModel createMathContext() {
    final result = ContextModel();

    forEach((tag) {
      result.customBind(tag.name, Number(tag.relevance));
    });

    return result;
  }
}

extension on TagDeclaration {
  Tag evaluateIn(ContextModel context) {
    final relevance = evaluateRelevanceIn(context);

    return Tag(name: name, relevance: relevance);
  }

  double evaluateRelevanceIn(ContextModel context) {
    return context.changeCopy((context) {
      if (context.hasNoVariable(name)) {
        context.customBind(name, Number(0));
      }

      return context.evaluate("$name + ${formatRelevance()}");
    });
  }

  String formatRelevance() {
    final trimmed = relevanceFormula.trim();

    return trimmed.isEmpty ? "1" : trimmed;
  }
}
