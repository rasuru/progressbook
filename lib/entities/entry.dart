import "package:app/utils.dart";

import "tag.dart";
import "tag_declaration.dart";

class Entry with EquatableMixin {
  Entry({
    required this.text,
    required this.timestamp,
  });

  final String text;
  final DateTime timestamp;

  String get title => text.title;
  Set<Tag> get tags => tagsIn(text);
  String get textBody =>
      text.and(withoutTitle).and(withoutTagLine).trim();

  @override
  String toString() {
    return "Entry from $timestamp";
  }

  @override
  List<Object?> get props => [timestamp];
}

String withoutTitle(String text) {
  return text.title.isEmpty ? text : text.dropFirstLine();
}

String withoutTagLine(String text) {
  return text.tagLine.isEmpty ? text : text.dropLastLine();
}

extension on String {
  String get title {
    final result = lines.first;

    return result.replaceWith("", when: result.length > 40);
  }

  List<String> get tagLine {
    final candidates = lines.last
        .trim()
        .and(shortenSequentialSpaces)
        .split(RegExp("^#| #"))
        .where((s) => s.isNotEmpty)
        .map((s) => "#$s")
        .toList();

    return candidates.every(checkIsValidTagCandidate)
        ? candidates
        : [];
  }
}
