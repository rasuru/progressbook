import "repositories/entry.dart";

Future deleteEntry(DateTime timestamp) async {
  await EntryRepo.removeBy(timestamp);
}
