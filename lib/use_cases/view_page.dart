import "package:app/entities/entry.dart";
import "package:app/utils.dart";

import "output/viewed_page.dart";
import "repositories/entry.dart";

StreamSubscription<Set<Entry>>? _subscription;

Future viewPage(DateTime pageDate) async {
  viewedPageDate$.update(pageDate);
  await _subscription?.cancel();
  _subscription =
      EntryRepo.whereDateIs(pageDate).listen(viewedPage$.update);

  return _subscription!.asFuture();
}
