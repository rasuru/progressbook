import "package:app/entities/tag_layer.dart";

import "repositories/tag_layer.dart";

Future editTagLayer(TagLayer layer) async {
  await TagLayerRepo.replaceBy(layer.id, layer);
}
