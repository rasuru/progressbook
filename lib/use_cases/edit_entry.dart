import "package:app/entities/entry.dart";
import "package:app/utils.dart";
import "package:app/utils/datetime.dart";

import "models/edited_entry.dart";
import "output/viewed_page.dart";
import "repositories/entry.dart";

export "models/edited_entry.dart";

Future editEntry(EditedEntry model) async {
  await EntryRepo.replaceBy(
    model.oldTimestamp,
    Entry(
      text: model.text,
      timestamp: model.time.withDateOf(current(viewedPageDate$)),
    ),
  );
}
