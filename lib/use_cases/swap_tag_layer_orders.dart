import "package:app/entities/tag_layer.dart";
import "package:app/utils.dart";

import "delete_tag_layer.dart";
import "edit_tag_layer.dart";
import "output/viewed_tag_layers.dart";
import "repositories/tag_layer.dart";

Future swapTagLayerOrders(int a, int b) async {
  final layerA = tagLayerOrdered(a);
  final layerB = tagLayerOrdered(b);

  await deleteTagLayer(layerA.id);
  await editTagLayer(layerB.copyWith(order: layerA.order));
  await TagLayerRepo.add(layerA.copyWith(order: layerB.order));
}

TagLayer tagLayerOrdered(int order) {
  return current(viewedTagLayers$)
      .singleWhere((layer) => layer.order == order);
}
