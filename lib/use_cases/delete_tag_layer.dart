import "package:app/utils.dart";

import "edit_tag_layer.dart";
import "get_tag_layer.dart";
import "output/viewed_tag_layers.dart";
import "repositories/tag_layer.dart";

Future deleteTagLayer(String id) async {
  final deletedOrder = (await getTagLayer(id)).order;

  await TagLayerRepo.removeBy(id);
  await current(viewedTagLayers$)
      .where((layer) => layer.order > deletedOrder)
      .map((layer) => layer.copyWith(order: layer.order - 1))
      .map(editTagLayer)
      .and((it) => Future.wait(it));
}
