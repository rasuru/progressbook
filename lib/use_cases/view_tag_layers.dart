import "package:app/entities/tag_layer.dart";
import "package:app/utils.dart";

import "output/viewed_tag_layers.dart";
import "repositories/tag_layer.dart";

StreamSubscription<Set<TagLayer>>? _subscription;

Future viewTagLayers() async {
  await _subscription?.cancel();
  _subscription = TagLayerRepo.streamAll().map((layers) {
    return layers.sorted((a, b) => a.order - b.order).toSet();
  }).listen(viewedTagLayers$.update);

  return _subscription!.asFuture();
}
