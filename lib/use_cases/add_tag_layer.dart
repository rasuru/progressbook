import "dart:math";

import "package:app/entities/tag_layer.dart";
import "package:app/utils.dart";

import "output/viewed_tag_layers.dart";
import "repositories/tag_layer.dart";

Future addTagLayer(String tagLayerText) async {
  await TagLayerRepo.add(
    TagLayer.generate(
      text: tagLayerText,
      order: getHighestTagLayerOrder().fold(() => 1, increment),
    ),
  );
}

Option<int> getHighestTagLayerOrder() {
  return current(viewedTagLayers$)
      .map((layer) => layer.order)
      .fold(0, max)
      .absentIf((it) => it == 0);
}
