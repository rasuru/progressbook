import "package:app/entities/tag_layer.dart";

import "repositories/tag_layer.dart";

Future<TagLayer> getTagLayer(String id) {
  return TagLayerRepo.getBy(id);
}
