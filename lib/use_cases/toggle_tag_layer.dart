import "edit_tag_layer.dart";
import "get_tag_layer.dart";

Future toggleTagLayer(String id) async {
  return getTagLayer(id)
      .then((layer) => layer.toggle())
      .then(editTagLayer);
}
