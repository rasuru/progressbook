import "package:app/utils.dart";
import "package:app/utils/datetime.dart";

import "output/viewed_page.dart";
import "view_page.dart";

Future viewPreviousPage() async {
  return viewPage(current(viewedPageDate$) - oneDay);
}
