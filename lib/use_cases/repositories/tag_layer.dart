import "package:app/entities/tag_layer.dart";

class TagLayerRepo {
  static late final Future Function(TagLayer) add;
  static late final Stream<Set<TagLayer>> Function() streamAll;
  static late final Future Function(String) removeBy;
  static late final Future Function(String, TagLayer) replaceBy;
  static late final Future<TagLayer> Function(String) getBy;

  static void init({
    required Future Function(TagLayer) add,
    required Stream<Set<TagLayer>> Function() streamAll,
    required Future Function(String) removeBy,
    required Future Function(String, TagLayer) replaceBy,
    required Future<TagLayer> Function(String) getBy,
  }) {
    TagLayerRepo.add = add;
    TagLayerRepo.streamAll = streamAll;
    TagLayerRepo.removeBy = removeBy;
    TagLayerRepo.replaceBy = replaceBy;
    TagLayerRepo.getBy = getBy;
  }
}
