import "package:app/entities/entry.dart";

class EntryRepo {
  static late final Future Function(Entry) add;
  static late final Stream<Set<Entry>> Function(DateTime)
      whereDateIs;
  static late final Future Function(DateTime) removeBy;
  static late final Future Function(DateTime, Entry) replaceBy;

  static void init({
    required Future Function(Entry) add,
    required Stream<Set<Entry>> Function(DateTime) whereDateIs,
    required Future Function(DateTime) removeBy,
    required Future Function(DateTime, Entry) replaceBy,
  }) {
    EntryRepo.add = add;
    EntryRepo.whereDateIs = whereDateIs;
    EntryRepo.removeBy = removeBy;
    EntryRepo.replaceBy = replaceBy;
  }
}
