import "package:app/entities/entry.dart";
import "package:app/utils.dart";
import "package:app/utils/datetime.dart";

import "models/new_entry.dart";
import "output/viewed_page.dart";
import "repositories/entry.dart";

export "models/new_entry.dart";

Future addEntry(NewEntry model) async {
  await EntryRepo.add(Entry(
    text: model.text,
    timestamp: model.time.withDateOf(current(viewedPageDate$)),
  ));
}
