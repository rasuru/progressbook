import "package:app/entities/tag.dart";
import "package:app/utils.dart";
import "package:app/entities/entry.dart";

final viewedPageDate$ = stateOf<DateTime>(DateTime.now());
final viewedPage$ = stateOf<Set<Entry>>({});
final viewedPageStats$ = viewedPage$.newState(
  (page) => page.fold<Set<Tag>>({}, (allTags, entry) {
    final result = <Tag>{...allTags};

    entry.tags.forEach((t) {
      if (result.contains(t)) {
        final old = result.lookup(t)!;
        result.remove(t);
        result.add(old.addRelevance(t.relevance));
      } else {
        result.add(t);
      }
    });

    return result
        .sorted((a, b) => a.relevance.compareTo(b.relevance))
        .toSet();
  }),
);
