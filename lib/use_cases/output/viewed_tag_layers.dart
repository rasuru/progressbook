import "package:app/entities/tag_layer.dart";
import "package:app/utils.dart";

final viewedTagLayers$ = stateOf<Set<TagLayer>>({});
final enabledTagLayers$ = viewedTagLayers$.newState(
  (layers) => layers.where((layer) => layer.enabled).toSet(),
);
