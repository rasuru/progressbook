class EditedEntry {
  EditedEntry({
    required this.text,
    required this.time,
    required this.oldTimestamp,
  });

  final String text;
  final DateTime time;
  final DateTime oldTimestamp;
}
