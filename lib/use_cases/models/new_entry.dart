class NewEntry {
  NewEntry({
    required this.text,
    required this.time,
  });

  final String text;
  final DateTime time;
}
