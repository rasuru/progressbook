import "dart:io";

import "package:intl/date_symbol_data_local.dart";

import "db/setup.dart" as db_setup;
import "repositories/entry.dart" as entry_repo;
import "repositories/tag_layer.dart" as tag_layer_repo;
import "ui/app.dart";
import "ui/utils.dart";
import "use_cases/view_page.dart";
import "use_cases/view_tag_layers.dart";

void main() async {
  initializeDateFormatting(Platform.localeName);
  initializeFlutter();
  await db_setup.open();
  entry_repo.init();
  tag_layer_repo.init();
  viewTagLayers();
  viewPage(DateTime.now());
  ProgressBook().run();
}
