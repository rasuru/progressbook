import "package:app/db/tables/entry.dart";
import "package:app/entities/entry.dart";
import "package:app/use_cases/repositories/entry.dart";

Future add(Entry entry) {
  return insertEntry(entry);
}

Stream<Set<Entry>> whereDateIs(DateTime date) {
  return readEntriesByDate(date);
}

Future removeBy(DateTime timestamp) {
  return deleteEntry(timestamp);
}

Future replaceBy(DateTime timestamp, Entry entry) {
  return updateEntry(timestamp, entry);
}

void init() {
  EntryRepo.init(
    add: add,
    removeBy: removeBy,
    replaceBy: replaceBy,
    whereDateIs: whereDateIs,
  );
}
