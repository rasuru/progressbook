import "package:app/db/tables/tag_layer.dart";
import "package:app/entities/tag_layer.dart";
import "package:app/use_cases/repositories/tag_layer.dart";

Future add(TagLayer tagLayer) {
  return insertTagLayer(tagLayer);
}

Stream<Set<TagLayer>> streamAll() {
  return readAllTagLayers();
}

Future removeBy(String id) {
  return deleteTagLayer(id);
}

Future replaceBy(String id, TagLayer tagLayer) {
  return updateTagLayer(id, tagLayer);
}

Future<TagLayer> getBy(String id) {
  return fetchTagLayer(id);
}

void init() {
  TagLayerRepo.init(
    add: add,
    removeBy: removeBy,
    replaceBy: replaceBy,
    streamAll: streamAll,
    getBy: getBy,
  );
}
