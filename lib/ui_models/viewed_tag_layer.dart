import "package:app/entities/tag_layer.dart";

class ViewedTagLayer {
  ViewedTagLayer({
    required this.id,
    required this.text,
    required this.enabled,
    required this.order,
  });

  static ViewedTagLayer from(TagLayer layer) {
    return ViewedTagLayer(
      id: layer.id,
      text: layer.text,
      enabled: layer.enabled,
      order: layer.order,
    );
  }

  final String id;
  final String text;
  final bool enabled;
  final int order;
}
