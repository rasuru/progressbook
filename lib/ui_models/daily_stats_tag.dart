import "package:app/entities/tag.dart";
import "package:app/utils.dart";

class DailyStatsTag {
  DailyStatsTag({
    required this.name,
    required this.relevance,
  });

  static DailyStatsTag from(Tag tag) {
    return DailyStatsTag(
      name: "${tag.name}(${tag.relevance.format()})",
      relevance: tag.relevance,
    );
  }

  final String name;
  final double relevance;
}
