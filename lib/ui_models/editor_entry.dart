import "package:app/utils.dart";

import "viewed_entry.dart";

abstract class EditorEntry {
  B useType<B>({
    required B Function() ifNew,
    required B Function(EditedEntry) ifEdited,
  });

  Option<DateTime> getLockedTime() => None();
}

class NewEntry extends EditorEntry {
  NewEntry() : super();

  @override
  B useType<B>({
    required ifNew,
    required ifEdited,
  }) =>
      ifNew();
}

class EditedEntry extends EditorEntry {
  EditedEntry({
    required this.text,
    required this.timestamp,
  });

  static EditedEntry from(ViewedEntry entry) {
    return EditedEntry(
      text: entry.text,
      timestamp: entry.timestamp,
    );
  }

  final DateTime timestamp;
  final String text;

  @override
  B useType<B>({
    required ifNew,
    required ifEdited,
  }) =>
      ifEdited(this);

  @override
  Option<DateTime> getLockedTime() => some(timestamp);
}
