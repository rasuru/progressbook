import "package:app/utils/datetime.dart";
import "package:app/utils.dart";
import "package:app/entities/entry.dart";
import "package:app/entities/tag.dart";

class ViewedEntry {
  ViewedEntry({
    required this.timestamp,
    required this.text,
    required this.timestampFormatted,
    required this.textBody,
    required this.title,
    required this.tagLine,
  });

  static ViewedEntry from(Entry entry) {
    final tagLine = entry.tags
        .map(formatTag)
        .join(" ")
        .absentIf((it) => it.isEmpty);

    return ViewedEntry(
      text: entry.text,
      timestamp: entry.timestamp,
      timestampFormatted: entry.timestamp.formatAsTime(),
      title: entry.title.absentIf(
        (it) => it.isEmpty || (tagLine.map(equals(it)) | false),
      ),
      textBody: entry.textBody.absentIf((it) => it.isEmpty),
      tagLine: tagLine,
    );
  }

  final DateTime timestamp;
  final String text;
  final String timestampFormatted;
  final Option<String> title;
  final Option<String> textBody;
  final Option<String> tagLine;
}

String formatTag(Tag tag) {
  final name = tag.name;
  final relevance = tag.relevance;

  if (relevance == 0) return "";
  if (relevance == 1) return "#$name";

  return "#$name(${relevance.format(precision: 2)})";
}
