import "dart:math";

import "package:dartz/dartz.dart";

export "dart:async" show StreamSubscription, FutureOr;

export "package:collection/collection.dart";
export "package:dartz/dartz.dart" hide State;
export "package:equatable/equatable.dart";
export "package:rxdart/rxdart.dart";
export "package:time/time.dart";

export "utils/option.dart";
export "utils/state.dart";

extension GeneralUtils<A> on A {
  B and<B>(B Function(A a) f) => f(this);

  A when(bool condition, A Function(A a) f) {
    return condition ? f(this) : this;
  }

  A replaceWith(A replacement, {required bool when}) =>
      this.when(when, (_) => replacement);

  A unless(bool condition, A Function(A a) f) {
    return condition ? this : f(this);
  }

  Option<A> absentIf(bool Function(A) fn) =>
      optionOf(nullifyIf(fn));

  A? nullifyIf(bool Function(A) fn) => fn(this) ? null : this;
}

A Function(A) unless<A>(bool condition, A Function(A a) f) {
  return (a) => condition ? a : f(a);
}

A Function(A) also<A>(Function f) {
  return (a) {
    if (f is Function(A)) {
      f(a);
    } else {
      f();
    }

    return a;
  };
}

bool Function(A) equals<A>(A first) {
  return (second) => first == second;
}

void Function(Object) printWith(String prefix) {
  return (it) => print("$prefix: $it");
}

extension StringUtils on String {
  List<String> get lines => split("\n");
  String dropFirstLine() => lines.drop().join("\n");
  String dropLastLine() => lines.dropLast().join("\n");
}

String shortenSequentialCharacters(String s, String char) {
  final cleared = s.replaceAll("$char$char", char);

  return s == cleared
      ? cleared
      : shortenSequentialCharacters(cleared, char);
}

String shortenSequentialSpaces(String s) {
  return shortenSequentialCharacters(s, " ");
}

extension NumUtils on num {
  num withPrecisionOf([int? n]) =>
      num.parse(toStringAsFixed(n ?? 2));

  String format({
    int? precision,
  }) {
    return this == toInt()
        ? toInt().toString()
        : withPrecisionOf(precision).toString();
  }
}

extension IntUtils on int {
  String toTwoDigitString() {
    return this >= 10 ? "$this" : "0$this";
  }
}

extension ListUtils<A> on List<A> {
  List<A> drop([int n = 1]) => sublist(min(length, n));
  List<A> dropLast([int n = 1]) =>
      sublist(0, max(0, length - n));
}

extension NullableListUtils<A> on List<A?> {
  List<A> withoutNulls() => whereType<A>().toList();
}

typedef Function1<A, B> = B Function(A);

extension Function1StreamUtils<A, B> on Stream<Function1<A, B>> {
  Stream<B> applyOn(A a) => map((fn) => fn(a));
}

bool itIs(bool a) => a;

N increment<N extends num>(N n) => (n + 1) as N;
