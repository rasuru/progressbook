import "../utils.dart";

extension OptionUtils<A> on Option<A> {
  A? get orNull => toNullable();

  A requireValue() => getOrElse(
        () => throw "Misused Option (no value)",
      );

  bool get isPresent => isSome();
  bool get isAbsent => isNone();

  void sideEffect(void Function(A) fn) => forEach(fn);
}

extension FutureOptionUtils<A> on Option<Future<A>> {
  Future<Option<A>> wait() => traverseFuture(id);
}
