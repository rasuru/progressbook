import "package:uuid/uuid.dart";

final uuid = Uuid();
final generateId = uuid.v4;
