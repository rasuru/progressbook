import "../utils.dart";

final oneDay = Duration(days: 1);

extension DateUtils on DateTime {
  bool isSameDateAs(DateTime date) {
    return year == date.year &&
        month == date.month &&
        day == date.day;
  }

  String formatAsTime() =>
      [hour, minute].map((n) => n.toTwoDigitString()).join(":");

  DateTime withTimeOf(DateTime time) =>
      time.copyWith(year: year, month: month, day: day);

  DateTime withDateOf(DateTime date) => copyWith(
        year: date.year,
        month: date.month,
        day: date.day,
      );
}

DateTime Function(DateTime) setHour(int hour) =>
    (dt) => dt.copyWith(hour: hour);

DateTime Function(DateTime) setMinute(int minute) =>
    (dt) => dt.copyWith(minute: minute);

DateTime Function(DateTime) setSecond(int second) =>
    (dt) => dt.copyWith(minute: second);

bool isCurrentYear(int year) {
  return DateTime.now().year == year;
}
