import "../utils.dart";

A current<A>(ValueStream<A> state) => state.value;

BehaviorSubject<A> every<A>(
  Duration duration,
  A Function() fn,
) =>
    Stream.periodic(duration, (_) => fn()).toState(fn());

BehaviorSubject<A> stateOf<A>([A? initialValue]) {
  return initialValue == null
      ? BehaviorSubject<A>(sync: true)
      : BehaviorSubject<A>.seeded(initialValue, sync: true);
}

extension StateUtils<A> on BehaviorSubject<A> {
  void update(A value) => add(value);
  void updateBy(A Function(A) fn) => add(fn(value));

  void updateIfChanged(A value) {
    if (this.value == value) return;
    update(value);
  }

  BehaviorSubject<B> newState<B>(B Function(A) fn) {
    final result = map(fn) as BehaviorSubject<B>;

    result.add(fn(value));

    return result;
  }

  BehaviorSubject<A> distinctState() {
    final result = distinct() as BehaviorSubject<A>;

    result.add(value);

    return result;
  }

  BehaviorSubject<A> copyState() => newState(id);
}

extension StreamUtils<A> on Stream<A> {
  StreamSubscription<A> trigger(Function() fn) =>
      listen((_) => fn());

  StreamSubscription<A> run() => listen(null);

  BehaviorSubject<A> toState(A? initially) {
    late BehaviorSubject<A> subject;
    late StreamSubscription<A> sub;

    subject = BehaviorSubject(
      onListen: () => sub = listen(
        subject.add,
        onError: subject.addError,
        onDone: subject.close,
      ),
      onCancel: () => sub.cancel(),
      sync: true,
    );

    if (initially != null) subject.add(initially);

    return subject;
  }
}
