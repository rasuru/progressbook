import "package:math_expressions/math_expressions.dart";

final antiClashPrefix = "_";
final parseMathExpression = Parser().parse;

extension ContextModelUtils on ContextModel {
  bool hasNoVariable(String variable, {bool noPrefix = false}) {
    final prefix = noPrefix ? "" : antiClashPrefix;

    return !variables.keys.contains(prefix + variable);
  }

  double evaluate(String expression) {
    expression = formatExpression(expression);

    if (expression.isEmpty) return 0;

    return parseMathExpression(expression)
        .evaluate(EvaluationType.REAL, this);
  }

  ContextModel copy() {
    final result = ContextModel();

    variables.forEach((variable, value) {
      result.bindVariableName(variable, value);
    });

    return result;
  }

  A changeCopy<A>(A Function(ContextModel copy) f) {
    final copy = this.copy();

    return f(copy);
  }

  void customBind(String name, Expression expression) {
    bindVariableName(antiClashPrefix + name, expression);
  }
}

String formatExpression(String expression) {
  return expression.trim().preventNameClashes();
}

extension _Expression on String {
  String preventNameClashes() {
    return replaceAllMapped(
      RegExp(
        r"(\p{L}|_)+",
        unicode: true,
      ),
      (match) => antiClashPrefix + match.group(0)!,
    );
  }
}
