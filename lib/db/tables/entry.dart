import "package:app/db/models/entry.dart";
import "package:app/db/utils.dart";
import "package:app/entities/entry.dart";
import "package:sqlbrite/sqlbrite.dart";

final insertEntry = database.insertEntry;
final deleteEntry = database.deleteEntry;
final updateEntry = database.updateEntry;

Stream<Set<Entry>> readEntriesByDate(DateTime date) {
  return database.createQuery(
    "entry",
    where: "timestamp BETWEEN DATE(?1) and DATE(?1, '+1 day')",
    whereArgs: [date.toSqlDate()],
  ).formatRows((row) => row.toEntry());
}

extension EntryTable on AbstractBriteDatabaseExecutor {
  Future insertEntry(Entry entry) {
    return insert("entry", entry.toModel().toRow());
  }

  Future deleteEntry(DateTime timestamp) {
    return delete(
      "entry",
      where: "timestamp = ?",
      whereArgs: [EntryModel.formatTimestamp(timestamp)],
    );
  }

  Future updateEntry(DateTime timestamp, Entry entry) {
    return update(
      "entry",
      entry.toModel().toRow(),
      where: "timestamp = ?",
      whereArgs: [EntryModel.formatTimestamp(timestamp)],
    );
  }
}
