import "package:app/db/models/tag_layer.dart";
import "package:app/db/utils.dart";
import "package:app/entities/tag_layer.dart";
import "package:sqlbrite/sqlbrite.dart";

final insertTagLayer = database.insertTagLayer;
final deleteTagLayer = database.deleteTagLayer;
final updateTagLayer = database.updateTagLayer;
final fetchTagLayer = database.fetchTagLayer;

Stream<Set<TagLayer>> readAllTagLayers() {
  return database
      .createQuery("tag_layer")
      .formatRows((row) => row.toTagLayer());
}

extension TagLayerTable on AbstractBriteDatabaseExecutor {
  Future insertTagLayer(TagLayer tagLayer) {
    return insert("tag_layer", tagLayer.toModel().toRow());
  }

  Future<TagLayer> fetchTagLayer(String id) async {
    return (await query(
      "tag_layer",
      where: "id = ?",
      whereArgs: [id],
    ))
        .first
        .toTagLayer();
  }

  Future deleteTagLayer(String id) {
    return delete(
      "tag_layer",
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future updateTagLayer(String id, TagLayer entry) {
    return update(
      "tag_layer",
      entry.toModel().toRow(),
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
