import "package:path/path.dart";
import "package:sqflite/sqflite.dart";
import "package:sqlbrite/sqlbrite.dart";

import "models/tag_layer.dart";
import "schemas/entry.dart";
import "schemas/tag_layer.dart";

late final BriteDatabase database;

Future<void> open() async {
  final directory = await getDatabasesPath();
  final file = join(directory, "progressbook.db");

  database = BriteDatabase(await openDatabase(
    file,
    version: 1,
    onCreate: populate,
  ));
}

Future populate(Database database, int version) {
  return database.transaction((t) async {
    await t.execute("PRAGMA foreign_keys = ON;");
    await t.execute(entryTableSchema);
    await t.execute(tagLayerTableSchema);
    await t.insert(
      "tag_layer",
      TagLayerModel(
        id: "example",
        text: "#a #b(2) #c(a * b)",
        enabled: true,
        order: 1,
      ).toRow(),
    );
  });
}
