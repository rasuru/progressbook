import "package:app/utils.dart";
import "package:sqlbrite/sqlbrite.dart";

export "setup.dart" show database;

extension QueryStream on Stream<Query> {
  Stream<Option<A>> collectRow<A>(
    Future<A> Function(Map) collect,
  ) {
    return formatRow(collect)
        .asyncMap((result) => result.wait());
  }

  Stream<Option<A>> formatRow<A>(A Function(Map) format) {
    return mapToOne(format)
        .map(optionOf)
        .onErrorReturnWith((error, _) {
      if (isNotFoundError(error)) return None();
      throw error;
    });
  }

  Stream<Set<A>> collectRows<A>(
    Future<A> Function(Map) collect,
  ) {
    return formatRows(collect)
        .asyncMap((rows) => Future.wait(rows))
        .asyncMap((list) => list.toSet());
  }

  Stream<Set<A>> formatRows<A>(A Function(Map) format) {
    return mapToList(format).map((list) => list.toSet());
  }
}

bool isNotFoundError(Object error) {
  return error is StateError &&
      error.message.contains("Query returned 0 row");
}

extension SqlDateTime on DateTime {
  String toSqlDateTime() => toIso8601String();
  String toSqlDate() => toSqlDateTime().substring(0, 10);
}
