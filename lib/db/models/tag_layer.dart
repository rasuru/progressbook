import "package:app/entities/tag_layer.dart";

class TagLayerModel {
  TagLayerModel({
    required this.id,
    required this.text,
    required this.enabled,
    required this.order,
  });

  TagLayer toEntity() {
    return TagLayer(
      id: id,
      text: text,
      enabled: enabled,
      order: order,
    );
  }

  final String id;
  final String text;
  final bool enabled;
  final int order;

  Map<String, dynamic> toRow() {
    return {
      "tag_layer_text": text,
      "tag_layer_order": order,
      "id": id,
      "enabled": enabled ? 1 : 0,
    };
  }
}

extension DbTagLayer on TagLayer {
  TagLayerModel toModel() {
    return TagLayerModel(
      id: id,
      text: text,
      enabled: enabled,
      order: order,
    );
  }

  Map toRow() => toModel().toRow();
}

extension TagLayerRow on Map {
  TagLayerModel toTagLayerModel() {
    return TagLayerModel(
      id: this["id"] as String,
      text: this["tag_layer_text"] as String,
      enabled: (this["enabled"] as int) == 1,
      order: this["tag_layer_order"] as int,
    );
  }

  TagLayer toTagLayer() => toTagLayerModel().toEntity();
}
