import "package:app/db/utils.dart";
import "package:app/entities/entry.dart";

class EntryModel {
  EntryModel({
    required this.timestamp,
    required this.text,
  });

  final String timestamp;
  final String text;

  Entry toEntity() {
    return Entry(
      text: text,
      timestamp: DateTime.parse(timestamp),
    );
  }

  Map<String, dynamic> toRow() {
    return {
      "entry_text": text,
      "timestamp": timestamp,
    };
  }

  static String formatTimestamp(DateTime ts) =>
      ts.toSqlDateTime();
}

extension DbEntry on Entry {
  EntryModel toModel() {
    return EntryModel(
      text: text,
      timestamp: EntryModel.formatTimestamp(timestamp),
    );
  }

  Map toRow() => toModel().toRow();
}

extension EntryRow on Map {
  EntryModel toEntryModel() {
    return EntryModel(
      text: this["entry_text"] as String,
      timestamp: this["timestamp"] as String,
    );
  }

  Entry toEntry() => toEntryModel().toEntity();
}
