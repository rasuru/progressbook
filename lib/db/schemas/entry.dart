final entryTableSchema = """
CREATE TABLE entry
( timestamp      TEXT    PRIMARY KEY
, entry_text     TEXT
);
""";
