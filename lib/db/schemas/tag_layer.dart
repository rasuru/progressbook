final tagLayerTableSchema = """
CREATE TABLE tag_layer
( id                 TEXT    PRIMARY KEY
, tag_layer_text     TEXT
, tag_layer_order    INT     UNIQUE
, enabled
     BOOLEAN
     NOT NULL
     CHECK (enabled IN (0, 1))
);
""";
