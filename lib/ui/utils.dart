import "package:app/utils.dart";
import "package:flutter/material.dart";
import "package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart";
import "package:snapping_sheet/snapping_sheet.dart";

export "package:app/utils.dart";
export "package:flutter/cupertino.dart" show CupertinoPicker;
export "package:flutter/material.dart"
    hide Notification, showTimePicker, showDatePicker;
export "package:line_awesome_flutter/line_awesome_flutter.dart";

export "widgets/change_notifier.dart";
export "widgets/column.dart";
export "widgets/decorations.dart";
export "widgets/lifecycle.dart";
export "widgets/list_view.dart";
export "widgets/loading_indicator.dart";
export "widgets/positioning.dart";
export "widgets/prompt.dart";
export "widgets/provider.dart";
export "widgets/row.dart";

late Size screenSize;

void setScreenSize(BuildContext context) {
  screenSize = getScreenSize(context);
}

final isKeyboardVisibleIn =
    KeyboardVisibilityProvider.isKeyboardVisible;

Widget buildIn(Widget Function(BuildContext) builder) {
  return Builder(builder: builder);
}

void initializeFlutter() {
  WidgetsFlutterBinding.ensureInitialized();
}

Widget disableBackButton(Widget child) {
  return WillPopScope(
    onWillPop: () async => false,
    child: child,
  );
}

extension SheetControllerUtils on SnappingSheetController {
  TickerFuture close() {
    return snapToPosition(
      SnappingPosition.factor(
        positionFactor: 0.0,
        grabbingContentOffset: GrabbingContentOffset.top,
      ),
    );
  }

  TickerFuture open() {
    return snapToPosition(
      SnappingPosition.factor(
        positionFactor: 1.0,
        grabbingContentOffset: GrabbingContentOffset.bottom,
      ),
    );
  }

  TickerFuture hide(double grabbingHeight) {
    return snapToPosition(
      SnappingPosition.pixels(
        positionPixels: -grabbingHeight,
        grabbingContentOffset: GrabbingContentOffset.top,
      ),
    );
  }
}

void runAfterBuild(Function() f) {
  WidgetsBinding.instance?.addPostFrameCallback((_) => f());
}

Widget Function(Widget) withAfterBuildCallback(
  Function(BuildContext) callback,
) {
  return (child) {
    return buildIn((context) {
      runAfterBuild(() => callback(context));
      return child;
    });
  };
}

Size getScreenSize(BuildContext context) {
  final mediaQuery = MediaQuery.of(context);
  final size = mediaQuery.size;
  final padding = mediaQuery.padding;
  final systemUiHeight = padding.top - padding.bottom;

  return Size(size.width, size.height - systemUiHeight);
}

extension UiDateTime on DateTime {
  TimeOfDay asTimeOfDay() => TimeOfDay.fromDateTime(this);
  String formatAsTime(BuildContext context) =>
      asTimeOfDay().format(context);
  Widget buildTimeLabel() => buildIn(
        (context) => Text(formatAsTime(context)),
      );
}

Widget Function(String) asUiText({
  TextStyle? style,
  TextAlign? align,
}) {
  return (text) => Text(
        text,
        style: style,
        textAlign: align,
      );
}

extension UiOption<A> on Option<A> {
  Widget build(Widget Function(A) builder) =>
      fold(() => Container(), builder);
}

extension TextControllerUtils on TextEditingController {
  int get cursorPosition => selection.base.offset;

  void appendText(String s) {
    final newText = (text.characters + s.characters).string;

    value = TextEditingValue(
      text: newText,
      selection: TextSelection.fromPosition(
        TextPosition(offset: text.length),
      ),
    );
  }
}
