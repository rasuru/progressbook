import "package:app/ui/utils.dart";

import "screens/diary/screen.dart";
import "theme.dart";

class ProgressBook extends StatelessWidget {
  @override
  Widget build(_) {
    theme$.add(ProgressBookTheme.generate());
    // This is for hot-reload

    return theme$.provide((theme) {
      return MaterialApp(
        title: "ProgressBook",
        home: buildIn((context) {
          setScreenSize(context);
          return buildDiaryScreen();
        }),
        debugShowCheckedModeBanner: false,
        theme: theme.material,
      );
    });
  }

  void run() => runApp(this);
}
