import "package:app/ui/utils.dart";
import "package:app/ui_models/viewed_entry.dart";
import "package:app/ui_presenters/entry_editor.dart"
    as entry_editor;
import "package:app/ui_presenters/viewed_page.dart";

import "action_area/state.dart";

Widget buildEntryTile(ViewedEntry entry) {
  return entry_editor.entry$.forceRebuilds(() {
    return wrapInColumn()([
      entry.buildHeading(),
      entry.textBody.build(asUiText(style: entryTextStyle)),
      SizedBox(height: 5),
      entry.tagLine.build(asUiText(style: tagLineTextStyle)),
    ])
        .and(addPadding(vertical: 10, horizontal: 20))
        .and(showSelectStatusOf(entry))
        .and(unless(
          entry_editor.isEditingExistingEntry,
          addGestureHandlersTo(entry),
        ));
  });
}

Widget Function(Widget) addGestureHandlersTo(ViewedEntry entry) {
  return (child) {
    return InkWell(
      onTap: () => toggleSelectEntry(entry),
      onLongPress: () {
        if (current(selectModeEnabled$)) {
          return toggleSelectEntry(entry);
        }

        halfOpenActionArea();
        entry_editor.edit(entry);
      },
      splashColor: Colors.grey.shade700,
      child: child,
    );
  };
}

Widget Function(Widget) showSelectStatusOf(ViewedEntry entry) {
  return (entryTile) {
    final isOpenInEditor = entry_editor.isEditing(entry);

    return selectedEntries$.forceRebuilds(() {
      return AnimatedContainer(
        color: isEntrySelected(entry) || isOpenInEditor
            ? Colors.grey.shade200
            : Colors.transparent,
        duration: 400.milliseconds,
        child: entryTile,
      );
    });
  };
}

extension on ViewedEntry {
  Row buildHeading() {
    return [
      Text(timestampFormatted, style: entryTimeStyle),
      SizedBox(width: 7),
      title
          .build(asUiText(style: entryTitleStyle))
          .and(expandWidget()),
      if (entry_editor.isEditing(this)) openInEditorStatusIcon,
    ].and(wrapInRow());
  }
}

final entryTimeStyle = TextStyle(
  fontSize: 13,
  color: Colors.grey.shade600,
);
final entryTitleStyle = TextStyle(
  fontSize: 13,
  fontWeight: FontWeight.w500,
);
final entryTextStyle = TextStyle(
  fontSize: 16,
  height: 1.4,
);
final tagLineTextStyle = TextStyle(
  color: Colors.grey.shade600,
);
final openInEditorStatusIcon = Icon(
  LineAwesomeIcons.alternate_feather,
  size: 16,
).and(setHeight(10));
