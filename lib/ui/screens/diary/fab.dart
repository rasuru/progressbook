import "package:app/ui/utils.dart";
import "package:app/ui_presenters/viewed_page.dart";
import "package:app/use_cases/delete_entry.dart";
import "package:flutter_speed_dial/flutter_speed_dial.dart";

Widget buildFAB() {
  return selectedEntries$.provide((entries) {
    return SpeedDial(
      visible: current(selectModeEnabled$),
      overlayColor: Colors.white30,
      children: [
        SpeedDialChild(
          onTap: disableSelectMode,
          label: "Отмена",
          labelStyle: TextStyle(color: Colors.white),
          child: Icon(LineAwesomeIcons.ban),
        ),
        SpeedDialChild(
          onTap: () {
            current(selectedEntries$)
                .map((entry) => entry.timestamp)
                .forEach(deleteEntry);
          },
          label: "Удалить",
          labelStyle: TextStyle(color: Colors.white),
          child: Icon(LineAwesomeIcons.trash),
        ),
      ],
      elevation: 0,
      activeIcon: LineAwesomeIcons.times,
      child: Text("${entries.length}", style: fabLabelStyle),
    );
  });
}

final fabLabelStyle = TextStyle(
  fontSize: 24,
  color: Colors.white,
);
