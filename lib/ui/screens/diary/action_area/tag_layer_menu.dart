import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";
import "package:app/ui/widgets/circle.dart";
import "package:app/ui_presenters/viewed_tag_layers.dart";
import "package:app/use_cases/add_tag_layer.dart";
import "package:app/use_cases/delete_tag_layer.dart";
import "package:app/use_cases/swap_tag_layer_orders.dart";
import "package:app/use_cases/toggle_tag_layer.dart";

final tagLayerTextController = TextEditingController();

Future showTagLayerPicker({
  required BuildContext context,
}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    barrierColor: Colors.white,
    builder: (dialogContext) {
      return Dialog(
        insetPadding: noPadding,
        child: buildTagLayerMenu(),
      );
    },
  );
}

Widget buildTagLayerMenu() {
  return [
    buildTagLayerList().and(expandWidget()),
    ColoredBox(color: Colors.grey.shade300)
        .and(setHeight(1))
        .and(addPadding(bottom: 8)),
    [
      TextField(
        controller: tagLayerTextController,
        decoration: InputDecoration(
          hintText: "#some #tags",
          filled: false,
        ),
      ).and(expandWidget()),
      SizedBox(width: 10),
      TextButton(
        onPressed: () async {
          await addTagLayer(tagLayerTextController.text);
          tagLayerTextController.clear();
        },
        style: TextButton.styleFrom(
          visualDensity: VisualDensity.compact,
        ),
        child: Icon(LineAwesomeIcons.plus),
      ),
    ]
        .and(wrapInRow(
          crossAxisAlignment: CrossAxisAlignment.stretch,
        ))
        .and(setHeight(36))
        .and(addPadding(horizontal: 10, bottom: 5)),
  ].and(wrapInColumn());
}

Widget buildTagLayerList() {
  return viewedTagLayers$.provide((layers) {
    return ReorderableListView(
      onReorder: (indexA, indexB) {
        swapTagLayerOrders(
          indexA + 1,
          indexB.when(indexB < indexA, increment),
        );
      },
      children: layers.map((layer) {
        return Container(
          key: ValueKey(layer.id),
          child: buildTagLayerTile(layer),
        );
      }).toList(),
    );
  });
}

Widget buildTagLayerTile(ViewedTagLayer layer) {
  return buildIn((context) {
    return ListTile(
      onTap: () => promptTagLayerAction(
        context: context,
        layer: layer,
      ),
      title: Text(layer.text),
      leading: Icon(LineAwesomeIcons.grip_lines, size: 16)
          .and(centerWidget())
          .and(setWidth(24)),
      trailing: buildEnabledIndicator(layer),
    );
  });
}

Widget buildEnabledIndicator(ViewedTagLayer layer) {
  return TextButton(
    onPressed: () => toggleTagLayer(layer.id),
    child: buildCircle(
      color: layer.enabled ? primaryColor : Colors.grey,
      size: 7,
    ).when(
      layer.enabled,
      addShadow(
        color: primaryColor.shade50,
        spreadRadius: 3,
        shape: BoxShape.circle,
      ),
    ),
  ).and(setWidth(32));
}

Future promptTagLayerAction({
  required BuildContext context,
  required ViewedTagLayer layer,
}) {
  return promptConfirmation(
    context: context,
    title: Text("Удалить слой тэгов?"),
    confirmButton: ElevatedButton.icon(
      onPressed: () async {
        await deleteTagLayer(layer.id);
        Navigator.of(context).pop();
      },
      icon: Icon(LineAwesomeIcons.trash),
      label: Text("Удалить"),
    ),
  );
}
