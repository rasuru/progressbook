import "package:app/ui/utils.dart";
import "package:app/ui_models/editor_entry.dart";
import "package:app/ui_presenters/entry_editor.dart"
    as entry_editor;
import "package:app/use_cases/add_entry.dart" as domain;
import "package:app/use_cases/edit_entry.dart" as domain;

import "entry_text_field.dart";
import "state.dart";
import "tag_layer_menu.dart";

Widget buildActionAreaBody() {
  return [
    buildEntryTextField().and(expandWidget()),
    SizedBox(height: 10),
    buildEntryEditorActions(),
    SizedBox(height: 10),
  ]
      .and(wrapInColumn())
      .and(addPadding(horizontal: 10))
      .and(avoidColumnOverflow())
      .and(fillWith(Colors.white));
}

Widget buildEntryEditorActions() {
  return entry_editor.entry$.provide((entry) {
    return [
      buildIn((context) {
        return TextButton(
          onPressed: () {
            showTagLayerPicker(context: context);
          },
          child: [
            Icon(LineAwesomeIcons.tags, size: 18),
            Icon(LineAwesomeIcons.layer_group, size: 18),
          ].and(wrapInRow()),
        );
      }),
      buildFinishEditingButton().and(expandWidget()),
      if (entry is EditedEntry)
        TextButton(
          onPressed: () {
            entry_editor.reset();
            sheetController.close();
          },
          child: Icon(LineAwesomeIcons.times),
        ),
    ].and(wrapInRow());
  });
}

ElevatedButton buildFinishEditingButton() {
  return ElevatedButton(
    onPressed: finishEditing,
    child: Icon(LineAwesomeIcons.alternate_feather),
  );
}

void finishEditing() async {
  await current(entry_editor.entry$).useType(
    ifNew: addEntry,
    ifEdited: applyChangesToEntry,
  );
  entryTextController.clear();
  entryFocusNode.unfocus();
  await sheetController.close();
}

Future applyChangesToEntry(EditedEntry editedEntry) async {
  await domain.editEntry(
    domain.EditedEntry(
      oldTimestamp: editedEntry.timestamp,
      text: entryTextController.text,
      time: entry_editor.time,
    ),
  );
  entry_editor.reset();
}

Future addEntry() {
  return domain.addEntry(domain.NewEntry(
    text: entryTextController.text,
    time: entry_editor.time,
  ));
}
