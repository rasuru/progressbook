import "dart:math";

import "package:app/ui/utils.dart";
import "package:app/use_cases/view_next_page.dart";
import "package:app/use_cases/view_previous_page.dart";

import "../state.dart";
import "state.dart";

final buttonStyle = TextButton.styleFrom(
  padding: paddingOf(horizontal: 30),
);

Widget buildPageButtons() {
  final opacity$ = Rx.combineLatest2(
    isPageEnd$.throttleTime(40.milliseconds, trailing: true),
    sheetPosition$.throttleTime(40.milliseconds),
    calculateOpacity,
  );

  return Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      buildPreviousPageButton(),
      SizedBox(height: 24, width: 1).and(fillWith(Colors.grey)),
      buildNextPageButton(),
    ],
  )
      .and(fillWith(Colors.grey.shade200))
      .and(animateOpacity(
        dontIgnoreTapsAfter: 0.1,
      ).using(opacity$))
      .and(roundWidget(all: 36))
      .and(setHeight(36))
      .and(centerWidget());
}

Widget buildNextPageButton() {
  return TextButton(
    onPressed: viewNextPage,
    style: buttonStyle,
    child: Icon(LineAwesomeIcons.alternate_long_arrow_right),
  );
}

TextButton buildPreviousPageButton() {
  return TextButton(
    onPressed: viewPreviousPage,
    style: buttonStyle,
    child: Icon(LineAwesomeIcons.alternate_long_arrow_left),
  );
}

double calculateOpacity(
  bool isPageEnd,
  double sheetPosition,
) {
  final opacityAtPageEnd = isPageEnd ? 1.0 : 0.3;
  final opacityWithSheet = 0.8 - sheetPosition;

  return min(
    opacityWithSheet,
    opacityAtPageEnd,
  ).clamp(0, 1);
}
