import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";
import "package:app/ui/widgets/date_picker.dart";
import "package:app/ui_presenters/viewed_page.dart";
import "package:app/use_cases/view_page.dart";

Widget buildHeaderDate() {
  return buildIn((context) {
    return TextButton(
      onPressed: () => showDatePicker(
        context: context,
        initially: current(viewedPageDate$),
        onApply: viewPage,
      ),
      style: buttonStyle,
      child: viewedPageDateFormatted$.provide(asUiText()),
    );
  });
}

ButtonStyle get buttonStyle {
  return TextButton.styleFrom(
    alignment: Alignment.centerLeft,
    padding: paddingOf(horizontal: 10),
  ).merge(theme.material.textButtonTheme.style!);
}
