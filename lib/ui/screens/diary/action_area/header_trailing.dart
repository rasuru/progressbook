import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";
import "package:app/ui/widgets/time_picker.dart";
import "package:app/ui_presenters/entry_editor.dart"
    as entry_editor;
import "package:app/ui_presenters/viewed_page.dart";

import "state.dart";

Widget buildHeaderTrailing() {
  final isActionAreaOpen$ = sheetPosition$
      .map((it) => sheetController.currentPosition > 100)
      .distinct()
      .throttleTime(40.milliseconds, trailing: true);

  Widget smoothCrossFade(Widget child) =>
      child.and(setHeight(36)).and(setWidth(80));

  return isActionAreaOpen$.provide((isOpen) {
    return AnimatedCrossFade(
      firstChild: buildHeaderPriority().and(smoothCrossFade),
      secondChild: buildEntryTimeButton().and(smoothCrossFade),
      crossFadeState: isOpen
          ? CrossFadeState.showSecond
          : CrossFadeState.showFirst,
      alignment: Alignment.centerRight,
      sizeCurve: Curves.easeInOut,
      duration: 200.milliseconds,
    );
  }, initially: false);
}

TextButton buildHeaderPriority() {
  return TextButton(
    onPressed: () {},
    style: TextButton.styleFrom(
      alignment: Alignment.centerRight,
      padding: paddingOf(horizontal: 10),
    ).merge(theme.material.textButtonTheme.style!),
    child: priorityFormatted$.provide(asUiText()),
  );
}

Widget buildEntryTimeButton() {
  final style = TextButton.styleFrom(
    alignment: Alignment.centerRight,
    padding: paddingOf(horizontal: 10),
  ).merge(theme.material.textButtonTheme.style!);

  return buildIn((context) {
    return TextButton(
      onPressed: () => showEntryTimePicker(context),
      onLongPress: entry_editor.resetTime,
      style: style,
      child: buildEntryTimeLabel(),
    );
  });
}

Widget buildEntryTimeLabel() {
  return entry_editor.lockedTime$.provide((lockedTime) {
    return lockedTime.fold(
      buildTickingEntryTime,
      buildLockedEntryTime,
    );
  });
}

void showEntryTimePicker(BuildContext context) {
  showTimePicker(
    context: context,
    initially: entry_editor.time,
    onApply: entry_editor.lockTime,
    onReset: entry_editor.resetTime,
  );
}

Widget buildTickingEntryTime() {
  return buildEvery(
    1.seconds,
    () => DateTime.now().buildTimeLabel(),
  );
}

Widget buildLockedEntryTime(DateTime time) {
  return [
    Icon(LineAwesomeIcons.lock, size: 16),
    SizedBox(width: 5),
    time.buildTimeLabel(),
  ].and(wrapInRow(
    mainAxisAlignment: MainAxisAlignment.center,
  ));
}
