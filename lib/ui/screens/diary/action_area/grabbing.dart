import "package:app/ui/utils.dart";

import "header.dart";
import "page_buttons.dart";

final grabbingHeight = 86.0;

Widget buildGrabbing() {
  return Stack(children: [
    buildPageButtons()
        .and(setPosition(top: 0, left: 0, right: 0)),
    buildActionAreaHeader()
        .and(setPosition(bottom: 0, left: 0, right: 0)),
  ]).and(setHeight(grabbingHeight));
}
