import "package:app/ui/utils.dart";
import "package:app/ui/widgets/drag_indicator.dart";

import "header_date.dart";
import "header_trailing.dart";

Widget buildActionAreaHeader() {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: [
      buildHeaderDate(),
      Spacer(),
      buildHeaderTrailing(),
    ],
  ).and(decorate);
}

Widget decorate(Widget actionArea) {
  return actionArea
      .and(addDragIndicator)
      .and(setHeight(36))
      .and(fillWith(Colors.white))
      .and(roundWidget(topLeft: 10, topRight: 10))
      .and(addShadow(
        color: Colors.grey.shade300,
        offset: Offset(0, -2),
        blurRadius: 8,
      ));
}
