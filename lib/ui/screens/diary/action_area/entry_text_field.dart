import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";
import "package:app/ui_models/editor_entry.dart";
import "package:app/ui_presenters/entry_editor.dart"
    as entry_editor;
import "package:app/ui_presenters/viewed_tag_layers.dart";

final entryFocusNode = FocusNode();
final entryTextController = TextEditingController();

Widget buildEntryTextField() {
  return initialTagLine$
      .provideTo(_textFieldBuilder)
      .and(fillWith(theme.inputColor))
      .and(roundWidget(all: 5))
      .and(syncEntryEditor);
}

Widget _textFieldBuilder(Option<String> initialTagLine) {
  return TextField(
    controller: entryTextController,
    maxLines: null,
    keyboardType: TextInputType.multiline,
    focusNode: entryFocusNode,
    decoration: InputDecoration(
      hintText: initialTagLine.map((s) => "\n\n$s").toNullable(),
    ),
    onChanged: autocompleteEntryText,
  );
}

void autocompleteEntryText(String entryText) {
  final controller = entryTextController;
  final tagLine = initialTagLine$.value;

  if (entryText.characters.length == 1) {
    final tagLinePrefix = entryText == "#" ? " " : "\n\n";

    tagLine
        .map((s) => tagLinePrefix + s)
        .sideEffect(controller.appendText);
  }

  if (tagLine.isPresent &&
      controller.cursorPosition == 0 &&
      entryText.trim() == tagLine.requireValue()) {
    controller.clear();
  }
}

Widget syncEntryEditor(Widget child) {
  return LifeCycle.subscription(
    stream: entry_editor.entry$,
    action: (EditorEntry entry) {
      entry.useType(
        ifNew: entryTextController.clear,
        ifEdited: (e) => entryTextController.text = e.text,
      );
    },
    child: child,
  );
}
