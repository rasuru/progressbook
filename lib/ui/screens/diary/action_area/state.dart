import "package:app/ui/utils.dart";
import "package:snapping_sheet/snapping_sheet.dart";

final sheetPosition$ = stateOf(0.0);
final sheetController = SnappingSheetController();
final snappings = [
  SnappingPosition.factor(
    positionFactor: 0.0,
    grabbingContentOffset: GrabbingContentOffset.top,
  ),
  SnappingPosition.factor(
    positionFactor: 0.5,
    grabbingContentOffset: GrabbingContentOffset.bottom,
  ),
  SnappingPosition.factor(
    positionFactor: 1.0,
    grabbingContentOffset: GrabbingContentOffset.bottom,
  ),
];

TickerFuture halfOpenActionArea() {
  return sheetController.snapToPosition(snappings[1]);
}

TickerFuture repositionActionArea() {
  return sheetController.open();
}
