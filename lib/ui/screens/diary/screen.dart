import "package:app/ui/utils.dart";
import "package:app/ui/widgets/scaffold.dart";
import "package:app/ui_models/viewed_entry.dart";
import "package:app/ui_presenters/viewed_page.dart";

import "action_area.dart";
import "daily_stats.dart";
import "entry_tile.dart";
import "fab.dart";
import "state.dart";

Widget buildDiaryScreen() {
  return viewedPage$
      .provide(buildEntryList)
      .and(addActionArea)
      .and(wrapInScaffold(floatingActionButton: buildFAB()));
}

Widget buildEntryList(List<ViewedEntry> entries) {
  final listPadding = paddingOf(top: 30);

  return buildListView(
    onScroll: () => diaryScroll$.update(
      diaryScrollController.offset,
    ),
    trailing: entries.isEmpty
        ? buildNoDailyStats()
        : buildDailyStats(),
    controller: diaryScrollController,
    padding: listPadding,
    items: entries,
    itemBuilder: buildEntryTile,
  );
}
