import "package:app/ui/utils.dart";

final diaryScroll$ = stateOf(0.0);
final diaryScrollController = ScrollController();
final isPageEnd$ = diaryScroll$.map((offset) {
  final maxScroll =
      diaryScrollController.position.maxScrollExtent;
  final tilEnd = maxScroll - current(diaryScroll$);

  return tilEnd < screenSize.height * 0.5;
}) as BehaviorSubject<bool>;
