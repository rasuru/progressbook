import "dart:math";

import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";
import "package:app/ui_models/daily_stats_tag.dart";
import "package:app/ui_presenters/viewed_page.dart";
import "package:syncfusion_flutter_charts/charts.dart";

Widget buildDailyStats() {
  return [
    SizedBox(height: screenSize.height),
    dailyStats$.provide((tags) {
      return SfCartesianChart(
        axisLabelFormatter: formatAxisLabels,
        primaryXAxis: CategoryAxis(interval: 1),
        series: [barSeriesFor(tags)],
      )
          .and(setHeight(max(100, tags.length * 40)))
          .and(centerWidget());
    }),
    SizedBox(height: 100),
  ].and(wrapInColumn(mainAxisSize: MainAxisSize.min));
}

BarSeries<DailyStatsTag, double> barSeriesFor(
    List<DailyStatsTag> tags) {
  return BarSeries<DailyStatsTag, double>(
    dataSource: tags,
    yValueMapper: (tag, _) => tag.relevance,
    xValueMapper: (_, i) => i.toDouble(),
    width: 0.5,
    pointColorMapper: (_, i) => HSVColor.fromColor(primaryColor)
        .withValue(1 - (i + 1) / tags.length)
        .toColor(),
    animationDuration: 0,
    dataLabelMapper: (tag, _) => tag.name,
    dataLabelSettings: DataLabelSettings(
      isVisible: true,
    ),
  );
}

ChartAxisLabel formatAxisLabels(details) {
  return ChartAxisLabel(
    details.orientation == AxisOrientation.horizontal
        ? details.text
        : "",
    null,
  );
}

Widget buildNoDailyStats() {
  return [
    SizedBox(height: screenSize.height),
    Image(
      image: AssetImage("assets/images/shrug.png"),
      width: 240,
    ).and(centerWidget()),
  ].and(wrapInColumn(mainAxisSize: MainAxisSize.min));
}
