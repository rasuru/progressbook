import "package:app/ui/utils.dart";
import "package:app/ui_presenters/viewed_page.dart";
import "package:snapping_sheet/snapping_sheet.dart";
import "package:snapping_sheet/src/sheet_position_data.dart";

import "action_area/body.dart";
import "action_area/grabbing.dart";
import "action_area/state.dart";

Widget addActionArea(Widget diary) {
  return SnappingSheet(
    controller: sheetController,
    onSheetMoved: updateSheetPosition,
    grabbingHeight: grabbingHeight,
    grabbing: buildGrabbing(),
    snappingPositions: snappings,
    sheetBelow: SnappingSheetContent(
      child: buildActionAreaBody(),
    ),
    child: manageActionAreaVisibility(diary),
  ).and(withAfterBuildCallback((context) {
    if (isKeyboardVisibleIn(context)) repositionActionArea();
  }));
}

void updateSheetPosition(SheetPositionData position) {
  sheetPosition$.update(
    position.relativeToSheetHeight,
  );
}

Widget manageActionAreaVisibility(Widget diary) {
  return LifeCycle.subscription(
    stream: selectModeEnabled$,
    action: (bool enabled) {
      if (enabled) {
        sheetController.hide(grabbingHeight);
      } else {
        sheetController.close();
      }
    },
    child: diary,
  );
}
