import "package:app/ui/utils.dart";

final theme$ = stateOf(ProgressBookTheme.generate());

class ProgressBookTheme {
  ProgressBookTheme({
    required this.primaryColor,
    required this.inputColor,
  });

  static ProgressBookTheme generate() {
    return ProgressBookTheme(
      primaryColor: materialColorFrom(Color(0xFF444444)),
      inputColor: Colors.grey.shade200,
    );
  }

  final MaterialColor primaryColor;
  final Color inputColor;

  ThemeData get material => generateMaterialTheme(
        primaryColor: primaryColor,
        inputColor: inputColor,
      );
}

ProgressBookTheme get theme => current(theme$);
MaterialColor get primaryColor => theme.primaryColor;

ThemeData generateMaterialTheme({
  required MaterialColor primaryColor,
  required Color inputColor,
}) {
  return ThemeData(
    primaryColor: primaryColor,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: primaryColor,
        elevation: 0,
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: primaryColor,
      ),
    ),
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: primaryColor,
      selectionHandleColor: primaryColor,
    ),
    textTheme: TextTheme(subtitle1: TextStyle(height: 1.4)),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: inputColor,
      contentPadding: paddingOf(
        horizontal: 8,
        top: 16,
        bottom: 4,
      ),
      border: OutlineInputBorder(
        borderRadius: createBorderRadius(all: 40),
        borderSide: BorderSide.none,
      ),
    ),
  );
}

MaterialColor materialColorFrom(Color color) {
  return MaterialColor(color.value, {
    50: tintColor(color, 0.5),
    100: tintColor(color, 0.4),
    200: tintColor(color, 0.3),
    300: tintColor(color, 0.2),
    400: tintColor(color, 0.1),
    500: tintColor(color, 0),
    600: tintColor(color, -0.1),
    700: tintColor(color, -0.2),
    800: tintColor(color, -0.3),
    900: tintColor(color, -0.4),
  });
}

int tintColorValue(int value, double factor) =>
    (value + ((255 - value) * factor)).round().clamp(0, 255);

Color tintColor(Color color, double factor) => Color.fromRGBO(
    tintColorValue(color.red, factor),
    tintColorValue(color.green, factor),
    tintColorValue(color.blue, factor),
    1);
