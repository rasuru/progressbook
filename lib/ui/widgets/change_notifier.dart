import "package:app/ui/utils.dart";

Widget provideChanges<A>({
  required ChangeNotifier notifier,
  required A Function() updateData,
  required Widget Function(A) builder,
  A? initialData,
}) {
  final data$ = stateOf<A>(initialData);

  void _updateData() {
    data$.update(updateData());
  }

  return LifeCycle(
    initState: () => notifier.addListener(_updateData),
    dispose: () => notifier.removeListener(_updateData),
    child: data$.provide(builder),
  );
}

Widget listenToChanges({
  required ChangeNotifier? notifier,
  required Function()? onData,
  required Widget child,
}) {
  if (notifier == null || onData == null) return child;

  return LifeCycle(
    initState: () => notifier.addListener(onData),
    dispose: () => notifier.removeListener(onData),
    child: child,
  );
}
