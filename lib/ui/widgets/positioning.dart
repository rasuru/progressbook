import "package:app/ui/utils.dart";

Expanded Function(Widget) expandWidget([int flex = 1]) {
  return (widget) {
    return Expanded(
      flex: flex,
      child: widget,
    );
  };
}

Align Function(Widget) alignWidget(
  Alignment alignment,
) {
  return (widget) {
    return Align(
      alignment: alignment,
      child: widget,
    );
  };
}

Align Function(Widget) centerWidget() {
  return (widget) {
    return Center(child: widget);
  };
}

Widget Function(Widget) setPosition({
  double? top,
  double? left,
  double? right,
  double? bottom,
  double? width,
  double? height,
}) {
  return (widget) {
    return Positioned(
      top: top,
      left: left,
      right: right,
      bottom: bottom,
      width: width,
      height: height,
      child: widget,
    );
  };
}
