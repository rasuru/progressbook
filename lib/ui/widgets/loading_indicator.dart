import "package:app/utils.dart";
import "package:app/ui/utils.dart";

Widget buildLoadingIndicator({
  double size = 64,
}) {
  return CircularProgressIndicator(strokeWidth: 2)
      .and(setWidth(size))
      .and(setHeight(size))
      .and(centerWidget());
}
