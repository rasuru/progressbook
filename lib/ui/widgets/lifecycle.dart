import "package:app/ui/utils.dart";

class LifeCycle<A> extends StatefulWidget {
  final Widget child;
  final A Function()? initState;
  final Function(A)? disposeState;
  final Function()? dispose;

  LifeCycle({
    required this.child,
    this.dispose,
    this.disposeState,
    this.initState,
  });

  static LifeCycle<StreamSubscription<A>> subscription<A>({
    required Stream<A> stream,
    required Function(A) action,
    required Widget child,
  }) {
    return LifeCycle(
      initState: () => stream.listen(action),
      disposeState: (sub) => sub.cancel(),
      child: child,
    );
  }

  @override
  LifeCycleState<A> createState() {
    return LifeCycleState();
  }
}

class LifeCycleState<A> extends State<LifeCycle<A>> {
  late final A disposable;

  LifeCycleState();

  @override
  void initState() {
    super.initState();

    if (widget.initState != null) {
      disposable = widget.initState!();
    }
  }

  @override
  void dispose() {
    widget.disposeState?.call(disposable);
    widget.dispose?.call();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => widget.child;
}
