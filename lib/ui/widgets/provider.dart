import "package:app/ui/utils.dart";

typedef DataBuilder<A> = Widget Function(A);

extension StreamProvider<A> on Stream<A> {
  Widget provide(
    DataBuilder<A> builder, {
    A? initially,
  }) {
    return StreamBuilder<A>(
      stream: this,
      initialData: initially,
      builder: (_, snap) {
        if (snap.data == null) return buildLoadingIndicator();

        return builder(snap.data!);
      },
    );
  }

  Widget forceRebuilds(Widget Function() builder) {
    return StreamBuilder(
      stream: this,
      builder: (_, __) => builder(),
    );
  }
}

extension WidgetStream<A> on Stream<Widget> {
  Widget build() => provide(id);
}

Widget buildEvery(
  Duration duration,
  Widget Function() builder,
) {
  return every(duration, builder).build();
}

extension ValueStreamProvider<A> on ValueStream<A> {
  Widget provide(DataBuilder<A> builder) {
    return StreamBuilder<A>(
      stream: this,
      initialData: valueOrNull,
      builder: (_, snap) {
        if (snap.data == null) return buildLoadingIndicator();

        return builder(snap.data!);
      },
    );
  }

  Widget provideTo(DataBuilder<A> builder) => provide(builder);
}

extension FutureProvider<A> on Future<A> {
  Widget provide(DataBuilder<A> builder) {
    return FutureBuilder<A>(
      future: this,
      builder: (_, snap) {
        if (snap.data == null) return buildLoadingIndicator();

        return builder(snap.data!);
      },
    );
  }

  Widget forceRebuild(Widget Function() builder) {
    return FutureBuilder<A>(
      future: this,
      builder: (_, snap) {
        if (snap.data == null) return buildLoadingIndicator();

        return builder();
      },
    );
  }
}

extension WidgetModifier1<A> on Widget Function(Widget) Function(
    A) {
  Widget Function(Widget) using(Stream<A> stream) {
    return (child) => stream.map(this).applyOn(child).build();
  }
}
