import "package:app/ui/utils.dart";
import "package:flutter/cupertino.dart";

Future promptConfirmation({
  required BuildContext context,
  required Widget title,
  required Widget confirmButton,
}) {
  return showDialog(
    context: context,
    barrierDismissible: true,
    builder: (context) {
      return AlertDialog(
        title: title,
        actions: [
          TextButton(
            onPressed: Navigator.of(context).pop,
            child: Text("Отмена"),
          ),
          confirmButton,
        ],
      );
    },
  );
}
