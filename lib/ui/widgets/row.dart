import "package:app/ui/utils.dart";

Row Function(List<Widget>) wrapInRow({
  Key? key,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  MainAxisSize mainAxisSize = MainAxisSize.max,
  CrossAxisAlignment crossAxisAlignment =
      CrossAxisAlignment.start,
  TextDirection? textDirection,
  VerticalDirection verticalDirection = VerticalDirection.down,
  TextBaseline? textBaseline,
}) {
  return (children) {
    return Row(
      key: key,
      mainAxisSize: mainAxisSize,
      textBaseline: textBaseline,
      mainAxisAlignment: mainAxisAlignment,
      verticalDirection: verticalDirection,
      crossAxisAlignment: crossAxisAlignment,
      textDirection: textDirection,
      children: children,
    );
  };
}
