import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";

Widget addDragIndicator(Widget child) {
  return Stack(
    children: [
      SizedBox(height: 5, width: 40)
          .and(fillWith(theme.inputColor))
          .and(roundWidget(all: 5))
          .and(centerWidget()),
      Positioned.fill(child: child),
    ],
  );
}
