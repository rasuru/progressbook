import "package:app/ui/theme.dart";
import "package:app/ui/utils.dart";
import "package:table_calendar/table_calendar.dart";

Future showDatePicker({
  required BuildContext context,
  required DateTime initially,
  required Function(DateTime) onApply,
  Function()? onReset,
}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    barrierColor: Colors.white,
    builder: (dialogContext) {
      final date$ = stateOf(initially);
      final isReset$ = stateOf(false);

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Spacer(),
          SizedBox(height: 80),
          buildCalendar(date$),
          SizedBox(height: 40),
          Spacer(),
          [
            TextButton(
              onPressed: Navigator.of(dialogContext).pop,
              child: Text("Отмена"),
            ),
            SizedBox(width: 20),
            ElevatedButton(
              onPressed: () {
                if (current(isReset$) && onReset != null) {
                  onReset();
                } else {
                  onApply(current(date$));
                }

                Navigator.of(dialogContext).pop();
              },
              style: ElevatedButton.styleFrom(
                padding: paddingOf(horizontal: 40),
              ),
              child: Text("Применить"),
            ),
            SizedBox(width: 20),
            TextButton(
              onPressed: () {
                date$.update(DateTime.now());
                isReset$.update(true);
              },
              child: Text("Сбросить"),
            ),
          ].and(wrapInRow(
            mainAxisAlignment: MainAxisAlignment.center,
          )),
          SizedBox(height: 40),
        ],
      )
          .and(centerWidget())
          .and((datePicker) => Material(child: datePicker));
    },
  );
}

Widget buildCalendar(BehaviorSubject<DateTime> date$) {
  return date$.provide((focusedDay) {
    return TableCalendar(
      locale: "ru",
      firstDay: DateTime(0),
      lastDay: DateTime(9999999),
      focusedDay: focusedDay,
      selectedDayPredicate: (x) => focusedDay == x,
      onDaySelected: (selected, _) => date$.update(selected),
      headerStyle: calendarHeaderStyle,
      calendarStyle: calendarStyleFor(focusedDay),
    );
  });
}

CalendarStyle calendarStyleFor(DateTime focusedDay) {
  final todayIsFocused =
      focusedDay.isAtSameDayAs(DateTime.now());

  return CalendarStyle(
    todayTextStyle: TextStyle(
      color: todayIsFocused ? Colors.white : Colors.black,
    ),
    selectedDecoration: BoxDecoration(
      shape: BoxShape.circle,
      color: primaryColor,
    ),
    todayDecoration: BoxDecoration(
      shape: BoxShape.circle,
      color:
          todayIsFocused ? primaryColor : primaryColor.shade50,
    ),
  );
}

final calendarHeaderStyle = HeaderStyle(
  leftChevronIcon: Icon(
    LineAwesomeIcons.caret_left,
  ),
  rightChevronIcon: Icon(
    LineAwesomeIcons.caret_right,
  ),
  titleCentered: true,
  formatButtonVisible: false,
);
