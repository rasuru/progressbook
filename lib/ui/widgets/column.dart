import "package:app/ui/utils.dart";

Column Function(List<Widget>) wrapInColumn({
  Key? key,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  MainAxisSize mainAxisSize = MainAxisSize.max,
  CrossAxisAlignment crossAxisAlignment =
      CrossAxisAlignment.stretch,
  TextDirection? textDirection,
  VerticalDirection verticalDirection = VerticalDirection.down,
  TextBaseline? textBaseline,
}) {
  return (children) {
    return Column(
      key: key,
      mainAxisSize: mainAxisSize,
      textBaseline: textBaseline,
      mainAxisAlignment: mainAxisAlignment,
      verticalDirection: verticalDirection,
      crossAxisAlignment: crossAxisAlignment,
      textDirection: textDirection,
      children: children,
    );
  };
}

Widget Function(Widget) avoidColumnOverflow({
  ScrollPhysics physics = const BouncingScrollPhysics(),
  ScrollController? controller,
}) {
  return (column) {
    return CustomScrollView(
      physics: physics,
      controller: controller,
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: column,
        ),
      ],
    );
  };
}
