import "package:app/ui/utils.dart";

Widget buildCircle({
  required Color color,
  required double size,
}) {
  return ColoredBox(color: color)
      .and(setHeight(size))
      .and(setWidth(size))
      .and(roundWidget(all: size));
}
