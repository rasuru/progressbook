import "package:app/ui/utils.dart";

final noPadding = paddingOf(all: 0);

ClipRRect Function(Widget) roundWidget({
  double all = 0,
  double? topLeft,
  double? topRight,
  double? bottomLeft,
  double? bottomRight,
}) {
  return (widget) {
    return ClipRRect(
      borderRadius: createBorderRadius(
        all: all,
        topLeft: topLeft,
        topRight: topRight,
        bottomLeft: bottomLeft,
        bottomRight: bottomRight,
      ),
      child: widget,
    );
  };
}

BorderRadius createBorderRadius({
  double all = 0,
  double? topLeft,
  double? topRight,
  double? bottomLeft,
  double? bottomRight,
}) {
  return BorderRadius.only(
    topLeft: Radius.circular(topLeft ?? all),
    topRight: Radius.circular(topRight ?? all),
    bottomLeft: Radius.circular(bottomLeft ?? all),
    bottomRight: Radius.circular(bottomRight ?? all),
  );
}

ColoredBox Function(Widget) fillWith(Color color) {
  return (widget) {
    return ColoredBox(color: color, child: widget);
  };
}

DecoratedBox Function(Widget) addShadow({
  Color color = Colors.black,
  Offset offset = Offset.zero,
  double blurRadius = 0,
  double spreadRadius = 0,
  BoxShape shape = BoxShape.rectangle,
}) {
  return addShadows([
    BoxShadow(
      color: color,
      offset: offset,
      blurRadius: blurRadius,
      spreadRadius: spreadRadius,
    ),
  ], shape: shape);
}

DecoratedBox Function(Widget) decorate(
  BoxDecoration decoration,
) {
  return (widget) {
    return DecoratedBox(
      decoration: decoration,
      child: widget,
    );
  };
}

DecoratedBox Function(Widget) addShadows(
  List<BoxShadow> shadows, {
  BoxShape shape = BoxShape.rectangle,
}) {
  return (widget) {
    return DecoratedBox(
      decoration: BoxDecoration(
        shape: shape,
        boxShadow: shadows,
      ),
      child: widget,
    );
  };
}

SizedBox Function(Widget) setHeight(double height) {
  return (widget) {
    return SizedBox(
      height: height,
      child: widget,
    );
  };
}

SizedBox Function(Widget) setWidth(double width) {
  return (widget) {
    return SizedBox(
      width: width,
      child: widget,
    );
  };
}

Widget Function(Widget) renderWhen(
  bool shouldRender, [
  Duration duration = const Duration(milliseconds: 100),
]) {
  return (widget) {
    return IgnorePointer(
      ignoring: !shouldRender,
      child: AnimatedOpacity(
        duration: duration,
        opacity: shouldRender ? 1 : 0,
        child: widget,
      ),
    );
  };
}

Widget Function(Widget) addPadding({
  double all = 0,
  double? horizontal,
  double? vertical,
  double? left,
  double? right,
  double? top,
  double? bottom,
}) {
  return (widget) {
    return Padding(
      padding: paddingOf(
        all: all,
        horizontal: horizontal,
        vertical: vertical,
        left: left,
        right: right,
        top: top,
        bottom: bottom,
      ),
      child: widget,
    );
  };
}

EdgeInsets paddingOf({
  double all = 0,
  double? horizontal,
  double? vertical,
  double? left,
  double? right,
  double? top,
  double? bottom,
}) {
  return EdgeInsets.all(all).copyWith(
    left: left ?? horizontal,
    right: right ?? horizontal,
    top: top ?? vertical,
    bottom: bottom ?? vertical,
  );
}

Widget Function(Widget) setOpacity(
  double opacity, {
  double dontIgnoreTapsAfter = 0,
}) {
  return (child) => IgnorePointer(
      ignoring: opacity < dontIgnoreTapsAfter,
      child: Opacity(opacity: opacity, child: child));
}

Widget Function(Widget) Function(double) animateOpacity({
  Duration duration = const Duration(milliseconds: 100),
  double dontIgnoreTapsAfter = 0,
}) {
  return (opacity) => (child) {
        return IgnorePointer(
          ignoring: opacity < dontIgnoreTapsAfter,
          child: AnimatedOpacity(
            opacity: opacity,
            duration: duration,
            child: child,
          ),
        );
      };
}
