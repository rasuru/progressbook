import "package:app/ui/utils.dart";
import "package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart";

Widget Function(Widget) wrapInScaffold({
  PreferredSizeWidget? appBar,
  Widget? floatingActionButton,
  bool? resizeToAvoidBottomInset,
}) {
  return (body) {
    return Scaffold(
      appBar: appBar,
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      floatingActionButton: floatingActionButton,
      body: SafeArea(
        child: DefaultTextStyle.merge(
          style: TextStyle(),
          child: KeyboardVisibilityProvider(child: body),
        ),
      ),
    );
  };
}
