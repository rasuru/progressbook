import "package:app/utils/datetime.dart";
import "package:app/ui/utils.dart";
import "package:day_night_time_picker/lib/daynight_banner.dart";

Future showTimePicker({
  required BuildContext context,
  required DateTime initially,
  required Function(DateTime) onApply,
  Function()? onReset,
}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    barrierColor: Colors.white,
    builder: (dialogContext) {
      final time$ = stateOf(initially);
      final isReset$ = stateOf(false);
      final hourController = FixedExtentScrollController(
        initialItem: current(time$).hour,
      );
      final minuteController = FixedExtentScrollController(
        initialItem: current(time$).minute,
      );
      final secondController = FixedExtentScrollController(
        initialItem: current(time$).second,
      );

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Spacer(),
          SizedBox(height: 80),
          ClipRect(
            child: time$.provide((time) {
              return DayNightBanner(
                hour: time.hour,
                displace: time.hour / 24,
              );
            }),
          ),
          SizedBox(height: 40),
          [
            Spacer(),
            CupertinoPicker(
              onSelectedItemChanged: (hour) {
                isReset$.update(false);
                time$.updateBy(setHour(hour));
              },
              looping: true,
              itemExtent: 40,
              scrollController: hourController,
              children: List.generate(24, id)
                  .map((hour) => hour.toTwoDigitString())
                  .map((hour) => Text(hour))
                  .map(centerWidget())
                  .toList(),
            ).and(expandWidget()),
            Text(":", style: TextStyle(fontSize: 28, height: 1)),
            CupertinoPicker(
              onSelectedItemChanged: (minute) {
                isReset$.update(false);
                time$.updateBy(setMinute(minute));
              },
              looping: true,
              itemExtent: 40,
              scrollController: minuteController,
              children: List.generate(60, id)
                  .map((minute) => minute.toTwoDigitString())
                  .map((minute) => Text(minute))
                  .map(centerWidget())
                  .toList(),
            ).and(expandWidget()),
            Text(":", style: TextStyle(fontSize: 28, height: 1)),
            CupertinoPicker(
              onSelectedItemChanged: (second) {
                isReset$.update(false);
                time$.updateBy(setSecond(second));
              },
              looping: true,
              itemExtent: 40,
              scrollController: secondController,
              children: List.generate(60, id)
                  .map((second) => second.toTwoDigitString())
                  .map((second) => Text(second))
                  .map(centerWidget())
                  .toList(),
            ).and(expandWidget()),
            Spacer(),
          ].and(wrapInRow(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
          )),
          Spacer(),
          [
            TextButton(
              onPressed: Navigator.of(dialogContext).pop,
              child: Text("Отмена"),
            ),
            SizedBox(width: 20),
            ElevatedButton(
              onPressed: () {
                if (current(isReset$) && onReset != null) {
                  onReset();
                } else {
                  onApply(current(time$));
                }

                Navigator.of(dialogContext).pop();
              },
              style: ElevatedButton.styleFrom(
                padding: paddingOf(horizontal: 40),
              ),
              child: Text("Применить"),
            ),
            SizedBox(width: 20),
            TextButton(
              onPressed: () {
                time$.update(DateTime.now());
                hourController.jumpToItem(
                  current(time$).hour,
                );
                minuteController.jumpToItem(
                  current(time$).minute,
                );
                secondController.jumpToItem(
                  current(time$).second,
                );
                isReset$.update(true);
              },
              child: Text("Сбросить"),
            ),
          ].and(wrapInRow(
            mainAxisAlignment: MainAxisAlignment.center,
          )),
          SizedBox(height: 40),
        ],
      ).and(centerWidget());
    },
  );
}
